// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  apiKey: 'AIzaSyCX1Ur1W1BeDWdbVdRwlcOFLk9D5N_R24o',
  authDomain: 'km-app-e3af6.firebaseapp.com',
  databaseURL: 'https://km-app-e3af6.firebaseio.com',
  projectId: 'km-app-e3af6',
  storageBucket: 'km-app-e3af6.appspot.com',
  messagingSenderId: '654050670319',
  appId: '1:654050670319:web:f6936b56e4f3c61aba0d1a',
  measurementId: 'G-XQGY918JNX'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
if (firebase.messaging.isSupported()) {
  const messaging = firebase.messaging();
}
